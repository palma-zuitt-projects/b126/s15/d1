// function bouncer(age){
// 	// if-statement :1 situation/condition
// 	// if-else statement: 2 situations/condtions
// 	if (age < 18){
// 		console.log("you can't come in");
// 	}
// 	else{
// 	console.log("Enjoy the night!");
// 	}
// }



/*
If score is 5 = S/"superb!"
If score is 3/4 = P/"Pass"
If score is 2 or less = F/"Fail"


*/
/*

function giveGrade(grade){
	if(grade === 5){
		console.log("You get an S")
	}else if(grade < 5 && grade >= 3 ){
		console.log("You get a P")
	}else{
		console.log ("You fail")
	}
}


function numberChecker(num){
	if(num % 2 === 1){
		console.log("ODD")
	}else{
		console.log("EVEN")
	}
}

*/


/*function register(password1, password2){
	if(password1.length < 6){
		alert("Password must be at least 6 chars")
	}else if(password1 !== password2){
		alert("Password must match")
	}else{
		alert("User Registration complete")
	}
}
*/
/*
If (condition)
{
	to to execute if condition is true
}
else if (condition2)
code to execute if condition 2 is true (can have many as you want)

else if 
code here will execute if none of the conditions above are true

*/

// Data validation = the concept of ensuring that data we receive from users is of the correct format



/*
function windReader(speed){
	if(speed <= 30) {
		console.log("Not a typhoon yet")
	}else if(speed <= 61){
		console.log("a tropical depression is detected")
	}else if(speed >= 62 && speed <= 88){
		console.log("a tropical storm is detected")
	}else if(speed >=89){
		console.log("a severe tropical storm is detected")
	}

}*/



// function sayNumber(){
// 	console.log(1)
// 	// console.log is only a message, you cannot do anything to it but read it
// }


// function returnNumber(){
// 	return 1
// 	// anything that is returned is still
// }


// let age = prompt("Enter your age")

// if(age < 18){
// 	alert("you cant come in")
// }else{
// 	alert("You can come in!")
// }

// Ternary Operator

/*function isUnderAge(age){
	return (age < 18) ? true : false;
}*

// Sytax:
//  (condition) ? expression1: expression2;
// Ternary operator checks if the condition is true.
// If yes, it will run expression1
// If no, it will run expression2


// Guideliens:

// Ternary operators are a short-hand way to write if-else statements,
and so are good for short statements.
// However, they are generally not as human 
readable, espcially when nested

/*
SWITCH STATEMENTS
if-else statement asks if a condition is true or false

*/

/*function getFruitByColor(color){

	switch(color){
		case "red":
			return "apple"
			break
		case "yellow":
			return "banana"	
			break	
		case "orange":
			return	"orange"
			break
		case "green":
			return "pear"
			break
		default:
			return "unknown fruit"	

	}
}*/



// TRY-CATCH-FINALLY STATEMENT
/*
function tryCatchIntensityAlert(){
	try{
		alertat("Tropical storm Detected")
	}

	catch(err){
		// Catch errors with 'try.' In this case the error is an unknown
		// fuction "alerat" and we can show the error message in alert.
		alert(err.message)
	}


	finally	{
		// Continue execution of code regardless of success or failure in try
		// try/catch will still work without ittry
		alert('Code below bad code still works')
	}
}
*/







// HOMEWORK



function greaterNum(num1, num2){
	if(num1 > num2){
		console.log (num1)
	}else if(num1 < num2){
		console.log (num2)
	}else{
		console.log("SAME VALUE")
	}
}




function getGreetbyLanguage(language){

	switch(language){
		case "French":
			return "Bonjour Monde!"
			break
		case "Japanese":
			return "Kon'nichiwa sekai!"	
			break	
		case "Spanish":
			return	"Hola Mundo!"
			break
		default:
			return "unknown language"	

	}
}

function pluralize(dog, num){
	if(num === 1 && dog === "dog" ){
	console.log("I have " + num + " dog")
	}else if(num >= 2 && dog === "dog"){
	console.log("I have " + num + " dogs")
	}
}


function yourGrade(num1){
	if(num1  89){	
	console.log("Your Grade is A!")
	}else if (num1 >80){
	console.log("you fail")
	}
}
